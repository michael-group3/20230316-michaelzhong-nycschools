package com.example.nycschools.service

import com.example.nycschools.di.Module
import com.example.nycschools.model.FailureCallException
import com.example.nycschools.model.NullResponseBodyException
import com.example.nycschools.viewmodel.ViewState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface SchoolsRepo {

    fun getSchools(): Flow<ViewState>
    fun getScores(): Flow<ViewState>
}

class SchoolsRepoImpl(
    private val serviceApi: ServiceApi
) : SchoolsRepo {
    override fun getSchools(): Flow<ViewState> = flow {
        emit(ViewState.LOADING)

        try {
            val schools = serviceApi.getSchools()
            if (schools.isSuccessful) {
                schools.body()?.let {
                    emit(ViewState.SCHOOLS(it))
                } ?: throw NullResponseBodyException("Schools are null")
            } else {
                throw FailureCallException(schools.errorBody()?.string())
            }
        } catch (e: Exception) {
            emit(ViewState.ERROR(e))
        }
    }

    override fun getScores(): Flow<ViewState> = flow {
        emit(ViewState.LOADING)

        try {
            val scores = serviceApi.getScores()
            if (scores.isSuccessful) {
                scores.body()?.let {
                    emit(ViewState.SCORES(it))
                } ?: throw NullResponseBodyException("Scores are null")
            } else {
                throw FailureCallException(scores.errorBody()?.string())
            }
        } catch (e: Exception) {
            emit(ViewState.ERROR(e))
        }
    }

}