package com.example.nycschools.service

import com.example.nycschools.model.School
import com.example.nycschools.model.Score
import retrofit2.Response
import retrofit2.http.GET

interface ServiceApi {

    @GET(SCHOOLS)
    suspend fun getSchools(): Response<List<School>>

    @GET(SCORES)
    suspend fun getScores(): Response<List<Score>>

    companion object {
        const val URL = "https://data.cityofnewyork.us/resource/"
        private const val SCHOOLS = "s3k6-pzi2.json"
        private const val SCORES = "f9bf-2cp4.json"
    }
}