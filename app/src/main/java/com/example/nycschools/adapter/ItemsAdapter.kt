package com.example.nycschools.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.databinding.NycItemBinding
import com.example.nycschools.model.School

class ItemsAdapter(
    private val schools: MutableList<School> = mutableListOf(),
    private val schoolClicked: (School) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {

    fun updateNewSchools(newSchools: List<School>) {
        schools.clear()
        schools.addAll(newSchools)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            NycItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return schools.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.populateSchool(schools[position], schoolClicked)
    }
}

class ViewHolder(
    private val binding: NycItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun populateSchool(school: School, clickAction: (School) -> Unit) {
        binding.nameSchool.text = school.name ?: "NO NAME AVAILABLE"
        binding.emailSchool.text = school.email ?: "NO EMAIL AVAILABLE"

        binding.detailsBtn.setOnClickListener { clickAction(school) }
    }
}